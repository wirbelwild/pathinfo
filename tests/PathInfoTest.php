<?php

/**
 * Bit&Black PathInfo – Easy handling of path information in an object-oriented way.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\PathInfo\Tests;

use BitAndBlack\PathInfo\PathInfo;
use Generator;
use PHPUnit\Framework\TestCase;

/**
 * Class PathInfoTest.
 *
 * @package Web2PrintTest\Tests\Helper
 */
class PathInfoTest extends TestCase
{
    public function testCanModify(): void
    {
        $file = '/path/to/my-file.jpg';
        $pathInfo = new PathInfo($file);

        self::assertSame(
            '/path/to',
            $pathInfo->getDirName()
        );

        self::assertSame(
            '/path/to/my-file.jpg',
            $pathInfo->getFilePath()
        );

        self::assertSame(
            'my-file.jpg',
            $pathInfo->getBaseName()
        );

        $pathInfo = $pathInfo->withFileNameAppended('-test');
        
        self::assertSame(
            'my-file-test.jpg',
            $pathInfo->getBaseName()
        );

        $pathInfoModified = $pathInfo->withExtension('tif');
        
        self::assertSame(
            'jpg',
            $pathInfo->getExtension()
        );
        
        self::assertSame(
            'tif',
            $pathInfoModified->getExtension()
        );

        $src = PathInfo::createFromFile('/path/to/my-file.mp4')
            ->withFileNameAppended('-processed')
            ->getFilePath()
        ;

        self::assertSame(
            '/path/to/my-file-processed.mp4',
            $src
        );
    }

    public function testCanHandleDotFiles(): void
    {
        $pathInfo = new PathInfo('/path/to/.htaccess');

        self::assertNull(
            $pathInfo->getFileName()
        );

        self::assertSame(
            '.htaccess',
            $pathInfo->getBaseName()
        );
    }

    public function testCanHandleFilesWithoutSuffix(): void
    {
        $pathInfo = new PathInfo('/path/to/file');

        self::assertNull(
            $pathInfo->getExtension()
        );

        self::assertSame(
            'file',
            $pathInfo->getBaseName()
        );
    }
    
    public function testCanClone(): void
    {
        $pathInfo1 = PathInfo::createFromFile('/path/to/file1.jpg');
        $pathInfo2 = $pathInfo1->withBaseName('file2');
        
        self::assertNotSame(
            $pathInfo1,
            $pathInfo2
        );
    }
    
    public function testReturnsNull(): void
    {
        $pathInfo = new PathInfo('');
        
        self::assertNull(
            $pathInfo->getDirName()
        );

        self::assertNull(
            $pathInfo->getExtension()
        );

        self::assertNull(
            $pathInfo->getFileName()
        );
        
        self::assertNull(
            $pathInfo->getFilePath()
        );
        
        self::assertNull(
            $pathInfo->getBaseName()
        );
    }

    /**
     * @dataProvider getFilesWithParameters
     * @param PathInfo $pathInfo
     * @param string $fileNameExpected
     * @param string $extensionExpected
     * @return void
     */
    public function testCanHandleFilesWithParameters(PathInfo $pathInfo, string $fileNameExpected, string $extensionExpected): void
    {
        self::assertSame(
            $fileNameExpected,
            $pathInfo->getFileName()
        );

        self::assertSame(
            $extensionExpected,
            $pathInfo->getExtension()
        );
    }

    public static function getFilesWithParameters(): Generator
    {
        yield [
            new PathInfo('my_image.jpg'),
            'my_image',
            'jpg',
        ];

        yield [
            new PathInfo('my_image.jpg?width=250'),
            'my_image',
            'jpg',
        ];

        yield [
            new PathInfo('my_image.jpg#width=250'),
            'my_image',
            'jpg',
        ];
    }
}
