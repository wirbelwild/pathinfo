[![PHP from Packagist](https://img.shields.io/packagist/php-v/bitandblack/pathinfo)](http://www.php.net)
[![Latest Stable Version](https://poser.pugx.org/bitandblack/pathinfo/v/stable)](https://packagist.org/packages/bitandblack/pathinfo)
[![Total Downloads](https://poser.pugx.org/bitandblack/pathinfo/downloads)](https://packagist.org/packages/bitandblack/pathinfo)
[![License](https://poser.pugx.org/bitandblack/pathinfo/license)](https://packagist.org/packages/bitandblack/pathinfo)

# Bit&Black PathInfo

Easy handling of path information in an object-oriented way.

## Installation

This library is made for the use with [Composer](https://packagist.org/packages/bitandblack/pathinfo). Add it to your project by running `$ composer require bitandblack/pathinfo`.

## Usage

Initialize the `PathInfo` with a path:

````php
<?php

use BitAndBlack\PathInfo\PathInfo;

$pathInfo = new PathInfo('/path/to/myfile.jpg');
````

You can access the single parts of the path now like that:

````php
<?php

/** Will echo "myfile" */
echo $pathInfo->getFileName();

/** Will echo "jpg" */
echo $pathInfo->getExtension();
````

You can also modify the parts:

````php
<?php

use BitAndBlack\PathInfo\PathInfo;

$pathInfo = new PathInfo('/path/to/myfile.jpg');

/** Will echo "/path/to/myfile.jpg" */
echo $pathInfo->getFilePath();

$pathInfoModified = $pathInfo
    ->withFileNameAppended('-modified')
    ->withExtension('png')
;

/** Will echo "/path/to/myfile-modified.png" */
echo $pathInfoModified->getFilePath();
````

## Help

If you have any questions, feel free to contact us under `hello@bitandblack.com`. 

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).