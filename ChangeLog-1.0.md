# Changes in Bit&Black PathInfo v1.0

## 1.0.3 2023-07-21

### Fixed

-   Fix bug with wrong extension when having parameter.

## 1.0.2 2023-07-19

### Fixed

-   Fix bug with wrong extension when having parameter.

## 1.0.1 2022-07-04

### Fixed

-   Fixed bug with wrong fullstops on missing extensions.