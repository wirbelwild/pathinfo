<?php

/**
 * Bit&Black PathInfo – Easy handling of path information in an object-oriented way.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\PathInfo;

/**
 * The PathInfo class allows the handle path information in an object-oriented way.
 */
class PathInfo
{
    /**
     * @var string|null
     */
    protected ?string $dirName = null;
    
    /**
     * @var string|null
     */
    protected ?string $extension = null;
    
    /**
     * @var string|null
     */
    protected ?string $fileName = null;

    /**
     * Creates a new instance from a given file.
     *
     * @param string|null $filePath
     */
    public function __construct(?string $filePath = null)
    {
        if (null === $filePath) {
            return;
        }
        
        if ('' !== $dirName = (string) pathinfo($filePath, PATHINFO_DIRNAME)) {
            $this->dirName = $dirName;
        }
        
        if ('' !== $extension = (string) pathinfo($filePath, PATHINFO_EXTENSION)) {
            $extensionParts = preg_split('/[?#]/', $extension);

            $this->extension = is_array($extensionParts)
                ? array_shift($extensionParts)
                : null
            ;
        }

        if ('' !== $fileName = (string) pathinfo($filePath, PATHINFO_FILENAME)) {
            $this->fileName = $fileName;
        }
    }

    /**
     * Creates a new instance from a given file.
     *
     * @param string $file
     * @return PathInfo
     */
    public static function createFromFile(string $file): PathInfo
    {
        return new PathInfo($file);
    }

    /**
     * Creates a new empty instance.
     *
     * @return PathInfo
     */
    public static function createNew(): PathInfo
    {
        return new PathInfo();
    }

    /**
     * Returns the file path. It contains the dir name and the base name.
     * For example `/path/to/my-file.jpg` will return `/path/to/my-file.jpg`.
     *
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->getFilePath();
    }

    /**
     * Returns the file path. It contains the dir name and the base name.
     * For example `/path/to/my-file.jpg` will return `/path/to/my-file.jpg`.
     *
     * @return string|null
     */
    public function getFilePath(): ?string
    {
        $dirName = $this->getDirName();
        $baseName = $this->getBaseName();
        
        if (null === $dirName && null === $baseName) {
            return null;
        }
        
        return $dirName . DIRECTORY_SEPARATOR . $baseName;
    }

    /**
     * Returns the whole information.
     *
     * @return array{
     *     dirname: string|null,
     *     basename: string|null,
     *     extension: string|null,
     *     filename: string|null,
     * }
     */
    public function getPathInfo(): array
    {
        return [
            'dirname' => $this->getDirName(),
            'basename' => $this->getBaseName(),
            'extension' => $this->getExtension(),
            'filename' => $this->getFileName(),
        ];
    }

    /**
     * Returns a new instance containing the given dir name.
     *
     * @param string $dirName
     * @return PathInfo
     */
    public function withDirName(string $dirName): self
    {
        $pathInfo = clone $this;
        $pathInfo->dirName = $dirName;
        return $pathInfo;
    }
    
    /**
     * Returns the dir name. For example `/path/to/my-file.jpg` will return `/path/to`.
     *
     * @return string|null
     */
    public function getDirName(): ?string
    {
        return $this->dirName;
    }

    /**
     * Returns a new instance containing the given base name.
     *
     * @param string $baseName
     * @return PathInfo
     */
    public function withBaseName(string $baseName): self
    {
        $baseNameInfo = self::createFromFile($baseName);
        
        return $this
            ->withFileName(
                (string) $baseNameInfo->getFileName()
            )
            ->withExtension(
                (string) $baseNameInfo->getExtension()
            )
        ;
    }

    /**
     * Returns the base name. It contains the file name and its extension.
     * For example `/path/to/my-file.jpg` will return `my-file.jpg`.
     *
     * @return string|null
     */
    public function getBaseName(): ?string
    {
        $fileName = $this->getFileName();
        $extension = $this->getExtension();

        if (null === $fileName && null === $extension) {
            return null;
        }

        return $fileName . (null !== $extension ? '.' . $extension : '');
    }

    /**
     * Returns a new instance containing the given extension.
     *
     * @param string $extension
     * @return PathInfo
     */
    public function withExtension(string $extension): self
    {
        $pathInfo = clone $this;
        $pathInfo->extension = $extension;
        return $pathInfo;
    }

    /**
     * Returns the extension. For example `/path/to/my-file.jpg` will return `jpg`.
     *
     * @return string|null
     */
    public function getExtension(): ?string
    {
        return $this->extension;
    }

    /**
     * Returns a new instance containing the given file name.
     *
     * @param string $fileName
     * @return PathInfo
     */
    public function withFileName(string $fileName): self
    {
        $pathInfo = clone $this;
        $pathInfo->fileName = $fileName;
        return $pathInfo;
    }

    /**
     * Returns the file name.
     *
     * @return string|null
     */
    public function getFileName(): ?string
    {
        return $this->fileName;
    }

    /**
     * Prepends a string to the file name.
     *
     * @param string $string
     * @return $this
     */
    public function withFileNamePrepended(string $string): self
    {
        $pathInfo = clone $this;
        $pathInfo->fileName = $string . $this->getFileName();
        return $pathInfo;
    }

    /**
     * Appends a string to the file name.
     *
     * @param string $string
     * @return $this
     */
    public function withFileNameAppended(string $string): self
    {
        $pathInfo = clone $this;
        $pathInfo->fileName = $this->getFileName() . $string;
        return $pathInfo;
    }
}
